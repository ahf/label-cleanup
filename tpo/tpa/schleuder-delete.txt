Name                           | Open       | Closed     | MR         | Delete    
component::internal services/schleuder |          0 |          0 |          0 | Delete    
owner::dgoulet                 |          0 |          0 |          0 | Delete    
owner::hiro                    |          0 |          0 |          0 | Delete    
parent::30898                  |          0 |          0 |          0 | Delete    
priority::high                 |          0 |          0 |          0 | Delete    
priority::medium               |          0 |          0 |          0 | Delete    
resolution::fixed              |          0 |          0 |          0 | Delete    
resolution::implemented        |          0 |          0 |          0 | Delete    
severity::normal               |          0 |          0 |          0 | Delete    
status::assigned               |          0 |          0 |          0 | Delete    
status::closed                 |          0 |          0 |          0 | Delete    
status::needs-information      |          0 |          0 |          0 | Delete    
status::new                    |          0 |          0 |          0 | Delete    
type::defect                   |          0 |          0 |          0 | Delete    
type::enhancement              |          0 |          0 |          0 | Delete    
type::task                     |          0 |          0 |          0 | Delete    
