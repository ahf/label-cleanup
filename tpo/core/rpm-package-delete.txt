Name                           | Open       | Closed     | MR         | Delete    
component::core tor/rpm packaging |          0 |          0 |          0 | Delete    
dependencies                   |          0 |          0 |          0 | Delete    
easy                           |          0 |          0 |          0 | Delete    
fedora                         |          0 |          0 |          0 | Delete    
milestone::Deliverable-December2010 |          0 |          0 |          0 | Delete    
milestone::Tor: 0.2.3.x-final  |          0 |          0 |          0 | Delete    
milestone::Tor: 0.3.1.x-final  |          0 |          0 |          0 | Delete    
owner::erinn                   |          0 |          0 |          0 | Delete    
owner::hiviah                  |          0 |          0 |          0 | Delete    
owner::marlowe                 |          0 |          0 |          0 | Delete    
packages                       |          0 |          0 |          0 | Delete    
packaging                      |          0 |          0 |          0 | Delete    
pie                            |          0 |          0 |          0 | Delete    
priority::high                 |          0 |          0 |          0 | Delete    
priority::low                  |          0 |          0 |          0 | Delete    
priority::medium               |          0 |          0 |          0 | Delete    
privoxy                        |          0 |          0 |          0 | Delete    
relro                          |          0 |          0 |          0 | Delete    
reporter::Pascal               |          0 |          0 |          0 | Delete    
reporter::hm2k                 |          0 |          0 |          0 | Delete    
reporter::januszeal            |          0 |          0 |          0 | Delete    
reporter::romaxion             |          0 |          0 |          0 | Delete    
reporter::tortux               |          0 |          0 |          0 | Delete    
resolution::duplicate          |          0 |          0 |          0 | Delete    
resolution::fixed              |          0 |          0 |          0 | Delete    
resolution::implemented        |          0 |          0 |          0 | Delete    
resolution::invalid            |          0 |          0 |          0 | Delete    
resolution::wontfix            |          0 |          0 |          0 | Delete    
resolution::worksforme         |          0 |          0 |          0 | Delete    
rhel                           |          0 |          0 |          0 | Delete    
rpm                            |          0 |          0 |          0 | Delete    
rust                           |          0 |          0 |          0 | Delete    
severity::normal               |          0 |          0 |          0 | Delete    
status::closed                 |          0 |          0 |          0 | Delete    
status::new                    |          0 |          0 |          0 | Delete    
tor-client                     |          0 |          0 |          0 | Delete    
type::defect                   |          0 |          0 |          0 | Delete    
type::enhancement              |          0 |          0 |          0 | Delete    
type::project                  |          0 |          0 |          0 | Delete    
type::task                     |          0 |          0 |          0 | Delete    
version::0.2.1.19              |          0 |          0 |          0 | Delete    
version::0.2.1.22              |          0 |          0 |          0 | Delete    
version::tor 0.2.3.15-alpha    |          0 |          0 |          0 | Delete    
version::tor 0.3.1.9           |          0 |          0 |          0 | Delete    
version::tor unspecified       |          0 |          0 |          0 | Delete    
