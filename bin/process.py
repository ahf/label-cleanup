#!/usr/bin/env python3
# Anyone receiving this file may do anything with it that copyright would
# otherwise restrict. There is no warranty.
"""
To use this script, write a configuration file like the one in
cfg/core_tor.cfg.  Give it a name ending with .cfg, like yourname.cfg.

Then, run:
   process.py yourname.cfg file1.yaml file2.yaml ...

It will generate file1.yaml.new, file2.yaml.new and so on, with labels
bulk-replaced in the way you have chosen.

Enjoy!
"""


import bisect
import re
import sys

def usage():
    print(__doc__.strip())

# ==============================
# This part is common to any file you're trying to edit.
# ==============================

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

def load_input(fn):
    data = load(open(fn), Loader=Loader)

    if isinstance(data, list):
        data = data[0]

    dispositions = dict()

    projname = data["project"]

    for kwd in data.get('delete') or []:
        dispositions[kwd] = ("delete", None)

    for kwd in data.get('unsorted') or []:
        dispositions[kwd] = ("unsorted", None)

    for kwd in data.get('save') or []:
        assert kwd not in dispositions
        dispositions[kwd] = ("save", kwd)

    for (kwd1,kwd2) in (data.get('reassign') or {}).items():
        assert kwd1 not in dispositions
        dispositions[kwd1] = ("save", kwd2)

    return (projname, dispositions)

def warn(s):
    print(s, file=sys.stderr)

class Dispositions:
    def __init__(self, fname):
        (proj, d) = load_input(fname)
        self._projname = proj
        self._d = d
        self._unsorted_keys = { k for k in d if d[k][0]=='unsorted' }

    def matching(self, pat):
        if isinstance(pat, re.Pattern):
            result = { k for k in self._unsorted_keys if pat.match(k) }
        elif pat in self._unsorted_keys:
            result = [pat]
        elif pat.endswith("*"):
            result = { k for k in self._unsorted_keys if k.startswith(pat[:-1]) }
        else:
            result = []

        return result

    def delete(self, pat):
        for k in self.matching(pat):
            self._d[k] = ("delete", None)
            self._unsorted_keys.remove(k)

    def save(self, pat):
        for k in self.matching(pat):
            self._d[k] = ("save", k)
            self._unsorted_keys.remove(k)

    def rename(self, pat, kwd2):
        for k in self.matching(pat):
            self._d[k] = ("reassign", kwd2)
            self._unsorted_keys.remove(k)

    def dump_unsorted(self):
        for k,disp in self._d.items():
            if disp[0] == "unsorted":
                print(k)

    def yaml_obj(self):
        obj = { }
        obj["project"] = self._projname
        obj["save"] = []
        obj["reassign"] = {}
        obj["delete"] = []
        obj["unsorted"] = []

        for kwd, (how, replacement) in self._d.items():
            if how == "reassign":
                obj["reassign"][kwd] = replacement
            else:
                obj[how].append(kwd)

        return [ obj ]

    def dump_yaml(self, into_file=None):
        if into_file == None:
            into_file = sys.stdout
        obj = self.yaml_obj()
        dump(obj, into_file, Dumper=Dumper, sort_keys=False)

class FileSet:
    def __init__(self, fnames):
        self._fs = {}
        for fn in fnames:
            self._fs[fn] = Dispositions(fn)

    def delete(self, kwd):
        for f in self._fs.values():
            f.delete(kwd)

    def save(self, kwd):
        for f in self._fs.values():
            f.save(kwd)

    def rename(self, kwd1, kwd2):
        for f in self._fs.values():
            f.rename(kwd1, kwd2)

    def dump_yaml(self):
        for (fname, disp) in self._fs.items():
            newfname = fname + ".new"
            with open(newfname, 'w') as f:
                warn("Writing to {}...".format(newfname))
                disp.dump_yaml(f)

inputfiles = sys.argv[1:]
if not inputfiles:
    warn("No input files given")
    usage()
    sys.exit(1)

if not inputfiles[0].endswith(".cfg"):
    warn("First filename didn't end with .cfg")
    warn("Expecting an executable python .cfg file as first argument")
    warn("see cfg/core_tor.cfg for an example.")
    sys.exit(1)

cfgfile = inputfiles[0]
inputfiles = inputfiles[1:]

if not inputfiles:
    warn("No input files given")
    usage()
    sys.exit(1)

fileset = FileSet(inputfiles)

configuration = open(cfgfile).read()
cfg_globals = {
    'delete': fileset.delete,
    'save': fileset.save,
    'rename': fileset.rename,
    }
exec(configuration, cfg_globals)

# ==============================
# Now we write the resulting structure to standard output.
# ==============================
fileset.dump_yaml()
