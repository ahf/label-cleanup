Gitlab Label Cleanup
====================

This Git repository contains one YAML encoded file per Gitlab project. Each
YAML file contains a list of labels used in the given project and each label
have a comment saying how many tickets in the given project that makes use of
the given label.
